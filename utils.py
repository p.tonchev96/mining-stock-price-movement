import pandas as pd
import matplotlib.pyplot as plt
import os
%matplotlib inline


############### Data Helpers ############### 
def load_data(csv_path):
    data = pd.read_csv(csv_path)
    data.rename(index=str,columns={ 'Unnamed: 0': 'Company'}, inplace=True)
    data.set_index('Company', inplace=True)
    data.columns = pd.to_datetime(data.columns)
    return data

def select_subset(dataframe,n_companies, n_days,total_companies):
    np.random.seed(47)
    rand_idx = np.random.choice(total_companies,n_companies, replace=False)
    subset = dataframe.iloc[rand_idx]
    return subset.iloc[:,:n_days]


############### Figures and Plotting ############### 
PROJECT_ROOT_DIR = "."
IMAGES_PATH = os.path.join(PROJECT_ROOT_DIR, "images")

def save_fig(fig_id, prefix='', tight_layout=False, fig_extension="png", resolution=300):
    
    dir_path = os.path.join(IMAGES_PATH, prefix)
    if not os.path.isdir(dir_path):
        os.makedirs(dir_path)
    
    path = os.path.join(dir_path, fig_id + "." + fig_extension)
    print("Saving figure", fig_id)
    if tight_layout:
        plt.tight_layout()
    plt.savefig(path, format=fig_extension, dpi=resolution)    


def save_clustering_results(X,n,labels, folder, plot_title, data_size='500x453'):
    current_path = os.path.join(folder,'{}_{}_clusters'.format(data_size,n))
    scaled_path = os.path.join(current_path, 'scaled')
    unscaled_path = os.path.join(current_path, 'unscaled')

    
    for label in np.unique(labels):
        X.iloc[labels == label].T.plot(figsize=(16,8),grid=True)
        plt.title(plot_title)
        plt.legend(loc='center left', bbox_to_anchor=(1.0, 0.5))
        save_fig(fig_id='clust_{}_{}'.format(label, data_size), prefix=unscaled_path, tight_layout=False, resolution=100)
        
        
    value_range = get_value_range(X)
    for label in np.unique(labels):
        X.iloc[labels == label].T.plot(figsize=(16,8),ylim=value_range,grid=True)
        plt.title(plot_title)
        plt.legend(loc='center left', bbox_to_anchor=(1.0, 0.5))
        save_fig(fig_id='clust_{}_{}'.format(label,data_size), prefix=scaled_path, tight_layout=False, resolution=100)
        
          
    clustered_df = pd.DataFrame(index=X.index, data=labels, columns=['Cluster'])
    clustered_df.index.set_names('company')
    clustered_df.to_csv(os.path.join(IMAGES_PATH, current_path + '/labels.csv'))

    
    
def get_value_range(dataframe):
    minValue = dataframe.T.describe().loc['min'].min()
    maxValue = dataframe.T.describe().loc['max'].max()
    return (minValue, maxValue)
    

############### Validation Helpers ############### 

from sklearn.preprocessing import MinMaxScaler

def scale_scores(score_df):
    scaler = MinMaxScaler()
    scaled_df = score_df.copy()
    scaled_df[scaled_df.columns] = scaler.fit_transform(score_df)
    return scaled_df

def plot_scores(score_df):
    scaled_df = scaled_scores(score_df)
    ax = scaled_df.plot(figsize=(16,8), x_compat=True, grid=True)
    ax.xaxis.set_major_locator(plt.MaxNLocator(scaled_df.index.max()))

def calc_combined_index(score_df):
    scaler = MinMaxScaler()
    scaled_df = score_df.copy()
    scaled_df[scaled_df.columns] = scaler.fit_transform(scaled_df)

    return scaled_df.calinski + scaled_df.silhuette + ( -1* scaled_df.davis)
