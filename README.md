# Mining Stock Prices for Better Investment

This project is tackling one of the most critical problems in modern finance, namely analysing the massive amount of valuable data in order to make investment decisions. An algorithmic approach would be beneficial for every investor and would potentially cut down the effort and cost of analytics. In this project, data mining techniques are employed to build an effcient portfolio for long-term investment. First, clustering algorithms are used to identify stocks sharing similar patterns in their price movement. After that, the formed clusters are used as building blocks for creating diversified portfolios. Moreover, a forecasting algorithm showed promising results when applied to portfolio optimisation. The resulting portfolios were able to beat the S&P500 Index fund by achieving a higher return, taking much less risk. This work was developed to serve as a proof-of-concept and potentially be used as a basis for developing more advanced investment tools.

For more details on the experiments and results please read the [report for the project](https://gitlab.com/p.tonchev96/mining-stock-price-movement/-/raw/master/PetarTonchev-ThirdYearReport.pdf).


## Experiments


| Algorithm	| Euclidean Distance| DTW  | Pearson Correlation |
|-----------|-------------------|------|---------------------|
|KMeans     |<ul><li>- [x] Clustering</li><li>- [x] Portfolio</li></ul>| *None*|*None*|
|KMediods   |<ul><li>- [x] Clustering</li><li>- [x] Portfolio</li></ul>|<ul><li>- [x] Clustering</li><li>- [x] Portfolio</li></ul>|<ul><li>- [x] Clustering</li><li>- [x] Portfolio</li></ul>|
|MeanShift  |<ul><li>- [x] Clustering</li><li>- [x] Portfolio</li></ul>| *None*|*None*|
|Agglomerative Complete  |<ul><li>- [x] Clustering</li><li>- [x] Portfolio</li></ul>|<ul><li>- [x] Clustering</li><li>- [x] Portfolio</li></ul>|<ul><li>- [x] Clustering</li><li>- [x] Portfolio</li></ul>|
|Agglomerative Average  |<ul><li>- [x] Clustering</li><li>- [x] Portfolio</li></ul>|<ul><li>- [x] Clustering</li><li>- [x] Portfolio</li></ul>|<ul><li>- [x] Clustering</li><li>- [x] Portfolio</li></ul>|
|Agglomerative Ward |<ul><li>- [x] Clustering</li><li>- [x] Portfolio</li></ul>| *None*|*None*|*None*|
|BIRCH   |<ul><li>- [x] Clustering</li><li>- [x] Portfolio</li></ul>| *None*|*None*|
|Affinity Propagation   |<ul><li>- [x] Clustering</li><li>- [x] Portfolio</li></ul>|<ul><li>- [x] Clustering</li><li>- [x] Portfolio</li></ul>|<ul><li>- [x] Clustering</li><li>- [x] Portfolio</li></ul>|
|HDBSCAN   |<ul><li>- [x] Clustering</li><li>- [x] Portfolio</li></ul>|<ul><li>- [x]  Clustering</li><li>- [x] Portfolio</li></ul>|<ul><li>- [x] Clustering</li><li>- [x] Portfolio</li></ul>|
|Three-Phase Clustering |*None*|<ul><li>- [x]  Clustering</li><li>- [x] Portfolio</li></ul>|*None*|


# Portfolio Selection

## Approach 1

1. Cluster the data
2. Select the stock with the highest Sharpe Ratio from each cluster
3. Optimize the portfolio

## Approach 2
1. Cluster the data
2. Rank the clusters depending on the mean Sharpe Ratio
3. Take clusters with good Sharpe Ratio
4. Select best n stocks
5. Optimize the portfolio


# Portfolio Validation

Run simulation for each portfolio and compare the returns with the S&P500 benchmark


